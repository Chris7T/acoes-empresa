<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Empresas</title>
    <link href="/css/styles.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>

<body>
    <div class="container-lg">
        <h1>
            <span class="badge bg-secondary">Empresas</span>
        </h1>

        <h1><span class="badge bg-secondary">Dados:</span></h1>

        <div>
            <label class="form-label">{{ __('Nome da Empresa: ') }}</label>
            <input class="form-control" type="text" value="{{ $empresa?->nomeEmpresa }}" disabled>
        </div>
        <div>
            <label class="form-label">{{ __('Moeda: ') }}</label>
            <input class="form-control" type="text" value="{{ $empresa?->moeda }}" disabled>
        </div>
        <div>
            <label class="form-label">{{ __('Média (30 dias): ') }}</label>
            <input class="form-control" type="text" value="{{ $empresa?->mediaVolume }}" disabled>
        </div>
        <div>
            <label class="form-label">{{ __('Simbolo: ') }}</label>
            <input class="form-control" type="text" value="{{ $empresa?->simbolo }}" disabled>
        </div>
        <div class="row">
            <div class="col">
                <label class="form-label">{{ __('Preço da ultima ação: ') }}</label>
                <input class="form-control" type="text" value="{{ $empresa?->ultimoValor }}" disabled>
            </div>
            <div class="col">
                <label class="form-label">{{ __('Preço MAX. ação (52 Semanas): ') }}</label>
                <input class="form-control" type="text" value="{{ $empresa?->maximo }}" disabled>
            </div>

            <div class="col">
                <label class="form-label">{{ __('Preço MIN. ação (52 Semanas): ') }}</label>
                <input class="form-control" type="text" value="{{ $empresa?->minimo }}" disabled>
            </div>
        </div>
        <br>
        <div>
            <a class="btn btn-light" href="/">Voltar</a>
        </div>
    </div>
</body>

</html>