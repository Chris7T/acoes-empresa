<?php

namespace Tests\Feature\Empresa;

use App\Models\Empresa;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Tests\TestCase;

class SalvarTest extends TestCase
{
    private const ROTA = 'empresa.buscar';
    private const FACEBOOK_SIMBOLO = 'fb';
    private const FACEBOOK_DADOS = ['nomeEmpresa' => 'Meta Platforms Inc - Class A'];

    public function testFalhaCampoObrigatorio()
    {
        $semSimbolo = [
            'simbolo' => null
        ];
        $response = $this->postJson(route(self::ROTA), $semSimbolo);

        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'simbolo',
                ],
            ]);
    }

    public function testFalhaSimboloInvalido()
    {
        $simboloInvalido = [
            'simbolo' => 123
        ];

        $this->postJson(route(self::ROTA), $simboloInvalido);

        Cache::shouldReceive('put')
            ->with('error');
    }

    public function testSucess()
    {
        $simboloFacebook = [
            'simbolo' => self::FACEBOOK_SIMBOLO
        ];

        $this->assertDatabaseMissing('empresas', self::FACEBOOK_DADOS);

        $response = $this->postJson(route(self::ROTA), $simboloFacebook);

        $response->assertViewIs('form');
        Http::shouldReceive('get');

        $this->assertDatabaseHas('empresas', self::FACEBOOK_DADOS);
    }

    public function testSucessDadosExistentes()
    {
        $simboloFacebook = [
            'simbolo' => self::FACEBOOK_SIMBOLO
        ];

        Empresa::factory()->create(self::FACEBOOK_DADOS);

        $this->assertDatabaseHas('empresas', self::FACEBOOK_DADOS);

        $response = $this->postJson(route(self::ROTA), $simboloFacebook);
        $response->assertViewIs('form');

        Http::shouldReceive('get');

        $this->assertDatabaseHas('empresas', self::FACEBOOK_DADOS)->count(1);
    }
}
