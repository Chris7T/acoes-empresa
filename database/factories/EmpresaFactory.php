<?php

namespace Database\Factories;

use App\Models\Empresa;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class EmpresaFactory extends Factory
{
    protected $model = Empresa::class;

    public function definition()
    {
        return [
            'nomeEmpresa' => $this->faker->company(),
            'moeda'       => $this->faker->currencyCode(),
            'mediaVolume' => $this->faker->numberBetween(0, 1000000),
            'simbolo'     => Str::random(3),
            'ultimoValor' => $this->faker->randomFloat(2, 0, 1000),
            'maximo'      => $this->faker->randomFloat(2, 0, 1000),
            'minimo'      => $this->faker->randomFloat(2, 0, 1000),
        ];
    }
}
