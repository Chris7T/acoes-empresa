<?php

namespace Tests\Feature\Empresa;

use Tests\TestCase;

class HomeTest extends TestCase
{
    public function testPaginaHome()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
        $response->assertViewIs('home');
    }
}
