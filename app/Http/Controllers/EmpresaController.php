<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use App\Http\Requests\EmpresaRequest;
use App\Models\Empresa;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use RegrasNegocio\IexRegras as RegrasIexApi;

class EmpresaController extends Controller
{
    public function home(): View
    {
        return view('home');
    }

    public function buscar(EmpresaRequest $request, RegrasIexApi $regras): RedirectResponse | View
    {
        $empresa = $regras->buscar($request->input('simbolo'));
        return $empresa
            ? view('form', ['empresa' => (object)$empresa])
            : redirect()->route('empresa.home');
    }
}
