<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'EmpresaController@home')->name('empresa.home')->middleware('web');
Route::post('/buscar', 'EmpresaController@buscar')->name('empresa.buscar')->middleware('web');
