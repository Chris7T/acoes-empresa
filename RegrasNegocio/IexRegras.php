<?php

namespace RegrasNegocio;

use App\Models\Empresa;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class IexRegras
{
    private const IEXAPI_URL = 'https://sandbox.iexapis.com/stable/stock/{?}/quote?token={?}';

    public function buscar(string $simbolo): Empresa | null
    {
        $empresa = $this->buscarEmpresa($simbolo);
        return $empresa ? $empresa : $this->buscarApi($simbolo);
    }

    private function buscarApi(string $simbolo): Empresa | null
    {
        $dados = Http::get(Str::replaceArray('{?}', [$simbolo, env('IEX_KEY')], self::IEXAPI_URL));
        if ($dados->failed() || !$dados->json()) {
            Cache::put('error', 'Simbolo inválido.', 5);
            return null;
        }
        Cache::forget('error');
        return $this->salvarEmpresa($dados->json());
    }

    private function salvarEmpresa(array $campos): Empresa
    {
        return Empresa::updateOrCreate(
            [
                'nomeEmpresa' => $campos['companyName']
            ],
            [
                'moeda'       => $campos['currency'],
                'mediaVolume' => $campos['avgTotalVolume'],
                'simbolo'     => Str::lower($campos['symbol']),
                'ultimoValor' => $campos['latestPrice'],
                'maximo'      => $campos['week52High'],
                'minimo'      => $campos['week52Low']
            ]
        );
    }

    private function buscarEmpresa(string $simbolo): Empresa | null
    {
        $empresa = Empresa::where('simbolo', $simbolo)->first();
        return $empresa
            ? $this->atualizar($empresa)
            : null;
    }

    private function atualizar(Empresa $empresa): Empresa | null
    {
        return Carbon::now()->diffInMinutes($empresa['updated_at']) > 10
            ? null
            : $empresa;
    }
}
