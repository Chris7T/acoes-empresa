<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Empresas</title>
    <link href="/css/styles.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>

<body>
    <div class="container-lg">
        <h1>
            <span class="badge bg-secondary">Empresas</span>
        </h1>
        <form class="formulario" method="POST" action="{{ route('empresa.buscar') }}">
            @csrf
            <div>
                <label class="form-label" for="simbolo">{{ __('Símbolo da Empresa: ') }}</label>
                <input class="form-control @if ($errors->any() or cache('error')) is-invalid @endif" type="text" id="simbolo" name="simbolo" value="{{ old('simbolo') }}">
                @error('simbolo')
                <span>
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
                <span>
                    <strong>{{ cache('error') }}</strong>
                </span>
            </div>
            <br>
            <div>
                <button class="btn btn-light" type="submit">Buscar</button>
            </div>
        </form>
    </div>
</body>

</html>