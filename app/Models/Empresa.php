<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    use HasFactory;

    protected $table = 'empresas';

    protected $fillable = [
        'nomeEmpresa',
        'moeda',
        'mediaVolume',
        'simbolo',
        'ultimoValor',
        'maximo',
        'minimo',
    ];
}
